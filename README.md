3D Metal Printer Simulator Server
=================================

[projectlink]:https://bitbucket.org/adamalpaca/printer3d_v2/src/master/
[localserver]:http://localhost:8080

The simulator is a part of the [3D metal printer project][projectlink]. 

This is a java project with a javascript user interface. Basically it starts a server that can serve to replace the 3D metal printing rig, in order to test the software.

It is used in the following way:

1. Launch the server
2. Go to the webpage at [localhost, port 8080][localserver]
3. Launch the client, the [3D metal printer project][projectlink]
4. Click on the button to send messgaes to the client